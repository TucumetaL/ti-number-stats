## Number stats

In order to work properly the script should be executed using Python 3.7 or a greater version.

To run: `python numbers_stats.py`

# Available commands:
~~~
`add <number>`: add a *number* to the collection
`build_stats`: build stats about numbers previously added
`less <number>`: shows how many numbers less than *number* were added
`greater <number>`: shows how many numbers greater than *number* were added
`between <lower_number> <greater_number>`: shows how many numbers between *lower_number* and *greater_number* were added
`help`: shows help
`exit`: exit program
~~~
