import shlex


class InvalidInput(Exception):
    pass


class Stats:

    def __init__(self, data):
        self.stats = dict()
        self.total = 0
        self._build_stats(data)


    def _build_stats(self, data):
        for i in range(len(data)):
            if data[i]:
                self.stats[i+1] = (self.total, data[i])
                self.total += data[i]

    def less(self, number):
        try:
            return self.stats[number][0]
        except KeyError:
            raise InvalidInput('Input must be a previously added number')

    def greater(self, number):
        try:
            return self.total - (self.stats[number][0] + self.stats[number][1])
        except KeyError:
            raise InvalidInput('Input must be a previously added number')

    def between(self, lower_number, greater_number):
        try:
            return self.stats[greater_number][0] + self.stats[greater_number][1] - self.stats[lower_number][0]
        except KeyError:
            raise InvalidInput('Input must be two previously added numbers')



class DataCapture:

    def __init__(self):
        self.numbers = [0] * 1000

    def add(self, number):
        if not (0 < number < 1000):
            raise InvalidInput('Input must be a positive integer less than 1000')
        self.numbers[number - 1] += 1

    def build_stats(self):
        return Stats(self.numbers)


def show_help():
    print('add <number>: add a number to the collection')
    print('build_stats: build stats about numbers previously added')
    print('less <number>: shows how many numbers less than `number` were added')
    print('greater <number>: shows how many numbers greater than `number` were added')
    print('between <lower_number> <greater_number>: shows how many numbers between'
        '\n   `lower_number` and `greater_number`were added')
    print('help: shows this help')
    print('exit: exit program')


if __name__ == '__main__':
    
    print('Enter a command to do something, e.g. `add <number>`.')
    print('To get help, enter `help`.')
    print('To exit, enter `exit`.')

    data = DataCapture()
    stats = None

    while True:
        cmd, *args = shlex.split(input('>>> '))

        if cmd == 'exit':
            break

        elif cmd == 'help':
            show_help()

        elif cmd == 'add':
            try:
                number = int(args[0])
                data.add(number)
            except (TypeError, ValueError):
                print('Input must be a positive integer less than 1000')
            except InvalidInput as e:
                print(e)
            else:
                print(f'Added "{number}"')

        elif cmd == 'build_stats':
            stats = data.build_stats()
            print('Stats has been built')

        elif cmd == 'less':
            if stats is None:
                print('You need to execute `build_stats` first to be able to use this command')
                continue
            try:
                number = int(args[0])
                print(f'There are {stats.less(number)} numbers less than {number}')
            except (TypeError, ValueError) as e:
                print('Input must be a positive integer less than 1000')
            except InvalidInput as e:
                print(e)

        elif cmd == 'greater':
            if stats is None:
                print('You need to execute `build_stats` first to be able to use this command')
                continue
            try:
                number = int(args[0])
                print(f'There are {stats.greater(number)} numbers greater than {number}')
            except (TypeError, ValueError):
                print('Input must be a positive integer less than 1000')
            except InvalidInput as e:
                print(e)

        elif cmd == 'between':
            if stats is None:
                print('You need to execute `build_stats` first to be able to use this command')
                continue
            try:
                lower_number, greater_number = args
                lower_number, greater_number = int(lower_number), int(greater_number)
                if not lower_number < greater_number:
                    print('First number must be less than second number')
                    continue
                print(f'There are {stats.between(lower_number, greater_number)} numbers between '
                    f'{lower_number} and {greater_number}')
            except (TypeError, ValueError):
                print('Input must be two positive integers less than 1000')
            except InvalidInput as e:
                print(e)

        else:
            print(f'Unknown command: {cmd}')
